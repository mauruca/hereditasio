# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.contrib import admin

from django.template import RequestContext
from django.conf.urls import *
from django.shortcuts import render_to_response
from django.contrib.admin.views.main import ChangeList

from simple_history.admin import SimpleHistoryAdmin
from import_export.admin import ImportExportModelAdmin, ExportMixin

# API Auth
from rest_framework.authtoken.admin import TokenAdmin

# Register your models here.
from .resources import *
from hereditascore.models import *
from hereditascore.services import *

#########################################
class LocalAdmin(ExportMixin, admin.ModelAdmin):
    list_display = ('id', 'nome', 'rfid', 'ativo')
    list_display_links = ('id', 'nome')
    list_filter = ['ativo']
    search_fields = ['nome']
    pass

class PadraoAdmin(ImportExportModelAdmin):
    resource_class = PadraoDepreciacaoResource
    list_display = ('id', 'conta', 'nome', 'vidautilmeses', 'percentualresidual', 'ativo')
    list_display_links = ('id', 'conta', 'nome')
    list_filter = ['ativo']
    search_fields = ['nome', 'conta']
    pass

class BemAdmin(ImportExportModelAdmin):
    resource_class = BemResource
    list_display = ('id', 'descricao', 'numerotombamento', 'dataaquisicao', 'valoratual', 'situacao', 'estado', 'rfid', 'ativo')
    list_display_links = ('id', 'descricao')
    list_filter = ['situacao','estado','ativo']
    search_fields = ['descricao', 'numerotombamento']
    actions = ['depreciarBens']

    def depreciarBens(self, request, queryset):
        for bem in queryset:
            dep = Depreciacao()
            dep.bem = bem
            dep.fazer()

    depreciarBens.short_description = "Depreciar Bens selecionados"

class InventarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'data', 'bem', 'local', 'user')
    list_display_links = ('id', 'data', 'bem')
    list_filter = ['data','local']
    pass

class DepreciacaoAdmin(admin.ModelAdmin):
    list_display = ('id', 'data', 'bem', 'valoranterior', 'valordepreciado')
    list_display_links = ('id', 'data', 'bem')
    list_filter = ['data']
    pass

class PessoaAdmin(ExportMixin, admin.ModelAdmin):
    list_display = ('id', 'user', 'rfid', 'ativo')
    list_display_links = ('id', 'user')
    list_filter = ['ativo']
    pass

#########################################
admin.site.register(Pessoa, PessoaAdmin)
admin.site.register(Local, LocalAdmin)
admin.site.register(Bem, BemAdmin)
admin.site.register(PadraoDepreciacao, PadraoAdmin)
admin.site.register(Depreciacao, DepreciacaoAdmin)
admin.site.register(Inventario, InventarioAdmin)
