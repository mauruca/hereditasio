# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from import_export import resources, fields
from import_export.widgets import *

from hereditascore.modelBem import *
from hereditascore.modelPadraoDepreciacao import *
from hereditascore.modelDepreciacao import *
from hereditascore.modelLocal import *

class BemResource(resources.ModelResource):
    valaquisicao = fields.Field(attribute='valoraquisicao',column_name='valaquisicao')
    valatual = fields.Field(attribute='valoratual',column_name='valatual')

    class Meta:
        model = Bem
        skip_unchanged = True
        report_skipped = False
        fields = ('id', 'descricao', 'numerotombamento', 'dataaquisicao', 'valaquisicao', 'valatual', 'situacao', 'estado', 'rfid', 'ativo')
        export_order = ('id', 'descricao', 'numerotombamento', 'dataaquisicao', 'valaquisicao', 'valatual', 'situacao', 'estado', 'rfid', 'ativo')


class PadraoDepreciacaoResource(resources.ModelResource):

    class Meta:
        model = PadraoDepreciacao
        skip_unchanged = True
        report_skipped = False

class LocalResource(resources.ModelResource):

    class Meta:
        model = Local
        skip_unchanged = True
        report_skipped = False
        fields = ('id', 'descricao')
