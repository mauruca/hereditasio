# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
ARG baseimage=python:alpine
FROM ${baseimage} as stage1

LABEL author="Mauricio Costa Pinheiro"
LABEL maintainer="maintainer@hereditas.in"

RUN apk update && apk add postgresql-dev gcc musl-dev libpq openssl bash

# Install requirements
COPY requirements requirements
RUN pip3 install --upgrade -r requirements

WORKDIR /usr/src/app

EXPOSE 8000

FROM stage1 as stage2

VOLUME ["/usr/src/app/static"]

# Copy static files
COPY ./static /usr/src/app/static

# Copy app
COPY ./io_run.sh /usr/src/app/
COPY ./*.py /usr/src/app/
COPY ./hereditasio /usr/src/app/hereditasio
COPY ./hereditascore /usr/src/app/hereditascore
COPY ./hereditasadmin /usr/src/app/hereditasadmin
COPY ./hereditasapi /usr/src/app/hereditasapi
COPY ./LICENSE* /usr/src/app/

# Create secret
RUN mkdir /etc/hereditas && openssl rand -base64 50 > /etc/hereditas/sk

# 0 - Creates administrator and execute and 1 - Normal execution
CMD ./io_run.sh 1 1

ENV LANG ${LANG}.UTF-8