#!/bin/sh

ver=expr psql -v ON_ERROR_STOP=1 -U $POSTGRES_USER -d postgres -c "SELECT datname FROM pg_database;" | grep $HER_DB_NAME
echo $ver

if [ -z $ver ];then
  psql -v ON_ERROR_STOP=1 -U $POSTGRES_USER -d postgres -c "CREATE DATABASE $HER_DB_NAME WITH ENCODING='UTF8' OWNER=$POSTGRES_USER CONNECTION LIMIT=-1;"
  psql -v ON_ERROR_STOP=1 -U $POSTGRES_USER -d postgres -c "GRANT ALL PRIVILEGES ON DATABASE $HER_DB_NAME TO $POSTGRES_USER"
fi