#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

ver="$(cat version)"

docker build $arq -t hereditas/pg:$ver .
docker images hereditas/pg
