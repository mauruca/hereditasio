#!/bin/sh
# Copyright (C) 2015-2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

# pip installed an upgraded in base server as virtualenv
echo " "
echo "upgrading pip...."
pip3 install --user --upgrade pip

echo "installing virtualenv...."
pip3 install --user virtualenv
