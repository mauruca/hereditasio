#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

ver="$(cat version)"

docker build --build-arg baseimage="$(cat baseimage)" -t hereditas/io:$ver .
docker images hereditas/io