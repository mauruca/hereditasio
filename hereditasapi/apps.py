# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.apps import AppConfig


class RestapiConfig(AppConfig):
    name = 'hereditasapi'
