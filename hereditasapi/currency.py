import locale
result = locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
if result == 'C':
    locale.setlocale(locale.LC_ALL, 'en_US')

def currency(value):
    if not value or value is None:
      return locale.currency(0, grouping=True)
    return locale.currency(value, grouping=True)
