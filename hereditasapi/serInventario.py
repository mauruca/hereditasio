# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *

class InventarioSerializer(serializers.ModelSerializer):
    #user = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Inventario
        fields = ('pk', 'data', 'bem', 'local', 'user')
        read_only_fields = ('pk',)
