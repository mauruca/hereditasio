# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *

class PessoaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pessoa
        fields = ('pk', 'rfid', 'user', 'nomecompleto', 'login')
        read_only_fields = ('pk', 'user', 'nomecompleto', 'login')
