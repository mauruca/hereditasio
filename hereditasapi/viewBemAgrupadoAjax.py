# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db.models import Count
from rest_framework import viewsets
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination

# Create your views here.
from .models import *
from .serializers import *
from .pagination import *
from .datatablesApiView import *

class BemAgrupadoAjaxViewSet(DatatablesAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Bem.objects.exclude(ativo=False).values('descricao').annotate(contagem=Count('descricao'))
    serializer_class = BemAgrupadoAjaxSerializer
