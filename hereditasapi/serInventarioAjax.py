# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *
from .currency import *

class InventarioAjaxSerializer(serializers.ModelSerializer):
    search_fields = ('id','data','bem__id','bem__descricao','local__id','local__nome','user__first_name','user__last_name')
    order_fields = ('id','data','bem__descricao','local__nome','user__first_name')

    bem = serializers.StringRelatedField(many=False,read_only=True)
    local = serializers.StringRelatedField(many=False,read_only=True)

    nomeCompleto = serializers.SerializerMethodField()
    def get_nomeCompleto(self,obj):
        return obj.user.get_full_name()

    class Meta:
        model = Inventario
        fields = ('id','data','bem','local','nomeCompleto')
        read_only_fields = ('id','data')
