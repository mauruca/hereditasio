# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

# Create your views here.
from .viewLocal import *
from .viewInventario import *
from .viewBem import *
from .viewBemAjax import *
from .viewBemAgrupadoAjax import *
from .viewInventarioAjax import *
from .viewDepreciacaoAjax import *
from .viewMovimentoBemAjax import *
from .viewMapaBemAjax import *
from .viewPessoa import *
from .viewMetricasAjax import *
