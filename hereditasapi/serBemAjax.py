# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *
from .currency import *


class BemAjaxSerializer(serializers.ModelSerializer):
    search_fields = ('id','descricao','numerotombamento','estado','estado','situacao',
        'dataaquisicao','datainiciouso','dataultimadepreciacao','valoraquisicao','valoratual','valorresidual')

    order_fields = ('id','descricao','numerotombamento','estado','estado','situacao',
        'dataaquisicao','datainiciouso','dataultimadepreciacao','valoraquisicao','valoratual','valorresidual','rfid')

    estadoDesc = serializers.SerializerMethodField()
    def get_estadoDesc(self,obj):
        return obj.get_estado_display()

    situacaoDesc = serializers.SerializerMethodField()
    def get_situacaoDesc(self,obj):
        return obj.get_situacao_display()

    servivel = serializers.SerializerMethodField()
    def get_servivel(self,obj):
        if obj.servivel():
            return "1"
        else:
            return "0"

    etiquetado = serializers.SerializerMethodField()
    def get_etiquetado(self,obj):
        if obj.temEtiqueta():
            return "1"
        else:
            return "0"

    valorAquisicaoLocale = serializers.SerializerMethodField()
    def get_valorAquisicaoLocale(self,obj):
        return currency(obj.valoraquisicao)

    valorAtualLocale = serializers.SerializerMethodField()
    def get_valorAtualLocale(self,obj):
        return currency(obj.valoratual)

    valorResidualLocale = serializers.SerializerMethodField()
    def get_valorResidualLocale(self,obj):
        return currency(obj.valorresidual)

    class Meta:
        model = Bem
        fields = ('id','descricao','numerotombamento','servivel','estadoDesc','situacaoDesc',
        'dataaquisicao','datainiciouso','dataultimadepreciacao','valoraquisicao','valorAquisicaoLocale','valoratual','valorAtualLocale','valorresidual','valorResidualLocale','rfid','etiquetado')
        read_only_fields = ('descricao','numerotombamento','estado','situacao',
        'dataaquisicao','datainiciouso','dataultimadepreciacao','valoraquisicao','valoratual','valorresidual','rfid',
        'valordepreciacaoacumulada','vidautilmeses','ativo','padraodepreciacao')
