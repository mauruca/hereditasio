# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import viewsets

# Create your views here.
from .models import *
from .serializers import *

class BemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Bem.objects.exclude(ativo=False).all()
    serializer_class = BemSerializer
