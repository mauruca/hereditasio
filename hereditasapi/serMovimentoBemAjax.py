# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *

class MovimentoBemAjaxSerializer(serializers.Serializer):
    idBem = serializers.IntegerField(read_only=True)
    descricaoBem = serializers.CharField(max_length=500,read_only=True)
    nTombamentoBem = serializers.CharField(max_length=15,read_only=True)
    dataultinv = serializers.DateTimeField(read_only=True)
    ultlocal = serializers.CharField(max_length=200,read_only=True)
    ultinventariante = serializers.CharField(max_length=60,read_only=True)
    datapenultinv = serializers.DateTimeField(read_only=True)
    penultlocal = serializers.CharField(max_length=200,read_only=True)
    penultimoinventariante = serializers.CharField(max_length=60,read_only=True)


    search_fields = ('id','descricao','numerotombamento','inventario__data','inventario__local__nome','inventario__user__first_name','inventario__user__last_name')
    order_fields = ('id','descricao','numerotombamento','inventario__data','inventario__local__nome','inventario__user__first_name','inventario__data','inventario__local__nome','inventario__user__first_name')
