# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import viewsets
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from django.db.models import Max, Count, F

# Create your views here.
from .models import *
from .serializers import *
from .pagination import *
from .datatablesApiView import *

def _get_dados(queryset):
    dados = []
    lastlocal = ""
    posicaoBemDetalhe = []
    posicaoBemitem = []
    local = ""
    for item in queryset:
        if lastlocal != item['local__id']:
            if len(posicaoBemDetalhe) > 0:
                posicaoBemitem.append(PosicaoBemItem(local, posicaoBemDetalhe))
            # novo local
            lastlocal = item['local__id']
            local = item['local__nome']
            # inicia lista de detalhes
            posicaoBemDetalhe = []

        posicaoBemDetalhe.append(PosicaoBemDetalhe(item['bem__numerotombamento']))

    if len(posicaoBemDetalhe) > 0:
        posicaoBemitem.append(PosicaoBemItem(local, posicaoBemDetalhe))

    pb = PosicaoBem(posicaoBemitem)
    return pb

class PosicaoBem:
    def __init__(self,listaItens):
        self.name = "base"
        self.children = listaItens

class PosicaoBemItem:
    def __init__(self,pname,listaDetalhe):
        self.name = pname # nome do local
        self.children = listaDetalhe

class PosicaoBemDetalhe:
    def __init__(self,pname):
        self.name = pname # numero tombamento
        self.size = 10

class MapaBemAPIView(GenericAPIView):
    def get(self, request, *args, **kwargs):
        self.pagination_class = APIResultsSetPagination
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.pagination_class = DatatablesResultsSetPagination
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()

        queryset = self.filter_queryset(queryset)

        dados = []
        dados = _get_dados(queryset)

        serializer = MapaBemAjaxSerializer(dados)
        return Response(serializer.data)

class MapaBemAjaxViewSet(MapaBemAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Inventario.objects.annotate(max_date=Max('bem__inventario__data')).filter(data=F('max_date')).values('bem__numerotombamento','local__id','local__nome').order_by('local__id')
    serializer_class = MapaBemAjaxSerializer
