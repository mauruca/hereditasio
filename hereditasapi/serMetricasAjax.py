# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
import locale
from django.db import models
from rest_framework import serializers
from .models import *
from .currency import *

class ContagemSerializer(serializers.Serializer):
    contagemBens = serializers.IntegerField(read_only=True, max_value=None, min_value=0)
    valorBens = serializers.FloatField(read_only=True)
    valorBensLocale = serializers.CharField(read_only=True)
    contagemBensEtiquetados = serializers.IntegerField(read_only=True, max_value=None, min_value=0)
    valorBensEtiquetados = serializers.FloatField(read_only=True)
    valorBensEtiquetadosLocale = serializers.CharField(read_only=True)
    contagemBensNaoEtiquetados = serializers.IntegerField(read_only=True, max_value=None, min_value=0)
    valorBensNaoEtiquetados = serializers.FloatField(read_only=True)
    valorBensNaoEtiquetadosLocale = serializers.CharField(read_only=True)
    contagemDepreciacoes = serializers.IntegerField(read_only=True, max_value=None, min_value=0)
    contagemInventarios = serializers.IntegerField(read_only=True, max_value=None, min_value=0)


class Contagem(models.Model):
    contagemBens = models.IntegerField()
    valorBens = models.FloatField()
    valorBensLocale = models.CharField(max_length=100)
    contagemBensEtiquetados = models.IntegerField()
    valorBensEtiquetados = models.FloatField()
    valorBensEtiquetadosLocale = models.CharField(max_length=100)
    contagemBensNaoEtiquetados = models.IntegerField()
    valorBensNaoEtiquetados = models.FloatField()
    valorBensNaoEtiquetadosLocale = models.CharField(max_length=100)
    contagemDepreciacoes = models.IntegerField()
    contagemInventarios = models.IntegerField()

    def __init__(self, contagemBens, valorBens, contagemBensEtiquetados, valorBensEtiquetados, contagemBensNaoEtiquetados, valorBensNaoEtiquetados, contagemDepreciacoes, contagemInventarios):
        self.contagemBens = contagemBens
        self.valorBens = valorBens
        self.valorBensLocale = currency(valorBens)
        self.contagemBensEtiquetados = contagemBensEtiquetados
        self.valorBensEtiquetados = valorBensEtiquetados
        self.valorBensEtiquetadosLocale = currency(valorBensEtiquetados)
        self.contagemBensNaoEtiquetados = contagemBensNaoEtiquetados
        self.valorBensNaoEtiquetados = valorBensNaoEtiquetados
        self.valorBensNaoEtiquetadosLocale = currency(valorBensNaoEtiquetados)
        self.contagemDepreciacoes = contagemDepreciacoes
        self.contagemInventarios = contagemInventarios
