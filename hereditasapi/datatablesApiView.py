from django.utils.datastructures import MultiValueDictKeyError
from django.db.models import Q
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from .pagination import *
from .currency import *

class DatatablesAPIView(GenericAPIView):
    """
    Concrete view for listing a queryset or creating a model instance.
    """

    def get(self, request, *args, **kwargs):
        self.pagination_class = APIResultsSetPagination
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.pagination_class = DatatablesResultsSetPagination
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        search = self._get_param_by_method(request,"search[value]")
        orderby = self._get_param_by_method(request,"order[0][column]")

        queryset = self.get_queryset()

        if search:
            queryset = queryset.filter(self.get_query(search))
        else:
            queryset = self.filter_queryset(queryset)

        if orderby:
            queryset = queryset.order_by(*self.get_orderby(request,orderby))

        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_query(self,search_term):
        query = None # Query to search for every search term
        for field in self.get_serializer().search_fields:
            q = Q(**{"%s__icontains" % field: search_term})
            if query is None:
                query = q
            else:
                query = query | q
        return query

    def get_orderby(self,request,colnum):
        idir = self._get_param_by_method(request,"order[0][dir]")
        diraux = ""
        if colnum is None:
            return ["-id"]
        if idir is not None:
            if idir == "desc":
                diraux = "-"
        icol = self._positive_int(colnum)
        column = self.get_serializer().order_fields[icol]
        retorno =  ["{}{}".format(diraux,column)]
        return retorno

    def _get_param_by_method(self,request,key):
        try:
            if request.method == 'GET':
                return request.query_params[key]
            elif request.method == 'POST':
                return request.data[key]
        except (MultiValueDictKeyError):
            return None

    def _positive_int(self,integer_string, strict=False, cutoff=None):
        """
        Cast a string to a strictly positive integer.
        """
        ret = int(integer_string)
        if ret < 0 or (ret == 0 and strict):
            raise ValueError()
        if cutoff:
            ret = min(ret, cutoff)
        return ret
