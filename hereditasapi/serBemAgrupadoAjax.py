# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *
from .currency import *


class BemAgrupadoAjaxSerializer(serializers.Serializer):
    contagem = serializers.IntegerField(read_only=True)
    descricao = serializers.CharField(max_length=500,read_only=True)

    search_fields = ('descricao',)
    order_fields = ('contagem','descricao')
