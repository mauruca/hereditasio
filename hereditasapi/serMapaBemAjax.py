# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *
from .currency import *

class PosicaoBemDetalheSerializer(serializers.Serializer):
    name = serializers.CharField()
    size = serializers.IntegerField()

class PosicaoBemItemSerializer(serializers.Serializer):
    name = serializers.CharField()
    children = serializers.ListField(child=PosicaoBemDetalheSerializer())

class MapaBemAjaxSerializer(serializers.Serializer):
    name = serializers.CharField(read_only=True)
    children = serializers.ListField(child=PosicaoBemItemSerializer(),read_only=True)
