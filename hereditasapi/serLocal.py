# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from rest_framework import serializers
from .models import *

class LocalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Local
        fields = ('pk','rfid','nome')
        read_only_fields = ('pk','nome',)
