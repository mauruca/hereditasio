# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db import models
from simple_history.models import HistoricalRecords

class PadraoDepreciacao(models.Model):
    conta = models.CharField('Conta',default="00000.00.00",max_length=11, blank=False)
    nome = models.CharField('Nome',max_length=150, blank=False)
    vidautilmeses = models.IntegerField('Vida útil em meses',default=60, blank=False)
    percentualresidual = models.IntegerField('Percentual residual',default=10, blank=False)
    #em uso
    ativo = models.BooleanField('Ativo',default=True)
    historico = HistoricalRecords(table_name='patrimonio_historicalpadraodepreciacao')

    def __str__(self):
        return self.nome + ' (vida de ' + str(self.vidautilmeses) + ' meses e residual de ' + str(self.percentualresidual) + ' %)'

    class Meta:
        verbose_name = 'Padrão de Depreciação'
        verbose_name_plural = 'Padrões de Depreciação'
        db_table = 'patrimonio_padraodepreciacao'
