import os, django
from patrimonio.models import Bem
from hereditas import settings          #your project settings file

from django.utils import timezone

def criaBem(qtd):
    #the list that will hold the bulk insert
    bulk_bem = []
    q = Bem.objects.all().order_by('-id')
    maxid = Bem.objects.all().order_by("-id")[0].id
    if q is None:
        return
    for i in range(qtd):
        for bem in q:
            maxid = maxid + 1
            new_bem = Bem()
            new_bem.descricao = bem.descricao
            new_bem.numerotombamento = maxid
            new_bem.dataaquisicao = timezone.now()
            new_bem.valoraquisicao = bem.valoraquisicao
            new_bem.valoratual = bem.valoratual
            new_bem.situacao = bem.situacao
            new_bem.estado = bem.estado
            bulk_bem.append(new_bem)
    Bem.objects.bulk_create(bulk_bem)
