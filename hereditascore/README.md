# Hereditas Core

Model django app for Hereditas platform. It is part of HereditasIO REST API.

## Getting Started

1. Add "hereditascore" to HereditasIO INSTALLED_APPS setting like this:

    INSTALLED_APPS = [
        ...
        'hereditascore',
    ]

2. Run `python manage.py migrate` to create the core models.
.