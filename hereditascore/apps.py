from django.apps import AppConfig


class HereditascoreConfig(AppConfig):
    name = 'hereditascore'
    verbose_name = 'Hereditas Core'
