# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db import models
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import User
from .modelBem import Bem
from .modelLocal import Local
from datetime import date

class Inventario(models.Model):
    data = models.DateTimeField('Data', blank=False)
    bem = models.ForeignKey(Bem, on_delete=models.PROTECT, verbose_name="Bem", blank=False)
    local = models.ForeignKey(Local, on_delete=models.PROTECT, verbose_name="Local", blank=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="Usuário", blank=False)
    #historico = HistoricalRecords(table_name='patrimonio_historicalinventario')

    def __str__(self):
        return self.bem.descricao + '(' + str(self.id) + ') ' + self.local.nome + ' ' + ' em %s/%s/%s ' % (self.data.day, self.data.month, self.data.year)

    class Meta:
        verbose_name = 'Inventário'
        verbose_name_plural = 'Inventários'
        db_table = 'patrimonio_inventario'
