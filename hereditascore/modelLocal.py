# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db import models
from simple_history.models import HistoricalRecords
from .modelConst import *
from .customFields.CharNullField import *

# Create your models here.
class Local(models.Model):
    nome = models.CharField('Nome', max_length=200, blank=False)
    #inventario
    rfid = CharNullField(LABEL_RFID, max_length=200, blank=True, null=True, unique=True)
    #em uso
    ativo = models.BooleanField('Ativo',default=True)
    historico = HistoricalRecords(table_name='patrimonio_historicallocal')

    def __str__(self):
        return self.nome + '(' + str(self.id) + ')'

    class Meta:
        verbose_name = 'Local'
        verbose_name_plural = 'Locais'
        db_table = 'patrimonio_local'
