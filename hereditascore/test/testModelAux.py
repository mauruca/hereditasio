# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
from django.test import TestCase, tag
from hereditascore.modelAux import Cpf, Cnpj

@tag('fast')
class CpfTest(TestCase):

    def test_format_14_chars(self):
        "format brazilian nif 14 chars"
        actual = Cpf.format(self,"11122233344555")
        expected = "111.222.333-44"
        self.assertEqual(actual, expected)

    def test_format_11_chars(self):
        "format brazilian nif 11 chars"
        actual = Cpf.format(self,"11122233344")
        expected = "111.222.333-44"
        self.assertEqual(actual, expected)

    def test_format_8_chars(self):
        "format brazilian nif 8 chars"
        actual = Cpf.format(self,"11122233")
        expected = "111.222.33-"
        self.assertEqual(actual, expected)

    def test_format_5_chars(self):
        "format brazilian nif 5 chars"
        actual = Cpf.format(self,"11122")
        expected = "111.22.-"
        self.assertEqual(actual, expected)

    def test_format_2_chars(self):
        "format brazilian nif 2 chars"
        actual = Cpf.format(self,"11")
        expected = "11..-"
        self.assertEqual(actual, expected)

    def test_validate_notvalid_nif(self):
        "check invalid brazilian nif"
        actual = Cpf.validate(self,"11122233344")
        expected = False
        self.assertEqual(actual, expected)

    def test_validate_valid_nif(self):
        "check valid brazilian nif"
        actual = Cpf.validate(self,"12358256390")
        expected = True
        self.assertEqual(actual, expected)

@tag('fast')
class CnpjTest(TestCase):

    def test_format_17_chars(self):
        "format brazilian entity nif 17 chars"
        actual = Cnpj.format(self,"11222333444455666")
        expected = "11.222.333/4444-55"
        self.assertEqual(actual, expected)

    def test_format_14_chars(self):
        "format brazilian entity nif 14 chars"
        actual = Cnpj.format(self,"11222333444455")
        expected = "11.222.333/4444-55"
        self.assertEqual(actual, expected)

    def test_format_8_chars(self):
        "format brazilian entity nif 8 chars"
        actual = Cnpj.format(self,"11222333")
        expected = "11.222.333/-"
        self.assertEqual(actual, expected)

    def test_validate_notvalid_nif(self):
        "check invalid brazilian entity nif"
        actual = Cnpj.validate(self,"11222333444455")
        expected = False
        self.assertEqual(actual, expected)

    def test_validate_valid_nif(self):
        "check invalid brazilian entity nif"
        actual = Cnpj.validate(self,"40271152000143")
        expected = True
        self.assertEqual(actual, expected)
