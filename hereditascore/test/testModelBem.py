# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
from datetime import date
from django.core.exceptions import ValidationError
from django.test import TestCase, tag
from hereditascore.modelBem import Bem

class BemTest(TestCase):

    def createBem(self, desc, tombamento, datainicio, prfid):
        return Bem(
            descricao = desc,
            numerotombamento = tombamento,
            rfid = prfid,
            dataaquisicao = date(2007, 12, 5),
            datainiciouso = datainicio,
            valoraquisicao = 1000.00,
            valoratual = 1000.00,
        )

    def test_create_bem_invalid(self):
        actual = self.createBem('teste bem',111222333444555666,date(2008, 1, 15), '123')
        with self.assertRaisesMessage(ValidationError, 'Ensure this value has at most 15 characters (it has 18).'):
            actual.clean_fields()

    @tag('slow', 'core')
    def test_save_bem(self):
        actual = self.createBem('teste bem',111222333444555,date(2008, 1, 15), '123')
        actual.save()
        expected = Bem.objects.filter(pk=actual.pk)[0]
        self.assertEqual(actual, expected)
