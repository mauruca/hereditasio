#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro.

# This is development script

apiport="4000"
pgaport="8000"
webport="8080"

Sintaxe(){
    echo "./runcompose.sh <db password> <hereditas admin password> <pgadmin password>"
    exit 1
}


if [ ! -z "$1" ]; then
    HER_DB_PW="$1"
else
    Sintaxe
fi

if [ ! -z "$2" ]; then
    HER_ADMIN_PW="$2"
else
    Sintaxe
fi

if [ ! -z "$3" ]; then
    PGADMIN_DEFAULT_PASSWORD="$3"
else
    Sintaxe
fi

# sec issues
export HER_DB_USER="herad"
export HER_DB_PW
export HER_SECPX="false"
export HER_ADMIN="had"
export HER_ADMIN_PW
export HER_PROD="false"
export HER_DEBUG="true"

# db issues - exporta para rodar os comandos na config dbadmshell e criar o banco de dados
export HER_USE_LITE="false"
export HER_DB_NAME="hereditas"
export HER_DB_HOST="hdb"

# hosts
export HER_APIPORT="$apiport"
export HER_WEBPORT="$webport"
export HER_HOSTSALLOWED="localhost"

export HER_GOOGLE_ANALYTICS=""
export HER_ADMIN_EMAIL="admin@hereditas.in"

# pgadmin
export PGADMIN_DEFAULT_EMAIL=$HER_ADMIN_EMAIL
export PGADMIN_DEFAULT_PASSWORD
export PGADMIN_WEBPORT="$pgaport"

docker network rm hereditasnet
docker network create hereditasnet

echo " "
echo "Starting Hereditas IO container @ http://localhost:$HER_APIPORT"
docker-compose -f compose.yml stop
docker-compose -f compose.yml up --force-recreate