#!/bin/sh

# Copyright (C) 2015-2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

echo " "
echo "starting virtualenv...."
. env/bin/activate
echo " "
echo "upgrading pip...."
pip3 install --upgrade pip
echo " "
echo "installing requirements...."
pip3 install --upgrade -r requirements
echo " "
echo "Defining session language...."
LANG=${LANG}
#LANG=pt_BR.UTF-8
echo
echo
echo "to activate the environment run command bash: source env/bin/activate or sh:. env/bin/activate"
