#!/bin/sh

# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

export HER_USE_LITE="true"
export HER_DB_NAME="hereditasdb"
export HER_DB_USER=""
export HER_DB_PW=""
export HER_DB_HOST=""
export HER_PROD="false"
export HER_DEBUG="true"
export HER_HOSTSALLOWED=""
export HER_GOOGLE_ANALYTICS=""
export HER_SECPX="false"
export HER_WEBPORT=""

echo -e "\ncreating secret file..."
openssl rand -base64 50 > sk
echo " "
echo "starting dev environment..."
. env/bin/activate

echo " "
echo "rebuilding static files..."
python3 manage.py collectstatic --noinput -c