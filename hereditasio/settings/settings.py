# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

import os

# Load defaults in order to then add/override with dev-only settings
from .default import *
from .language import *

# timezone
TIME_ZONE = 'UTC'
USE_TZ = False

#enviroment variables
E_DB_NAME = "postgres"
if os.environ.get('HER_DB_NAME'):
    E_DB_NAME = os.environ['HER_DB_NAME'].strip()
E_DB_USER = ""
if os.environ.get('HER_DB_USER'):
    E_DB_USER = os.environ['HER_DB_USER'].strip()
E_DB_PW = ""
if os.environ.get('HER_DB_PW'):
    E_DB_PW = os.environ['HER_DB_PW'].strip()
E_DB_HOST = "localhost"
if os.environ.get('HER_DB_HOST'):
    E_DB_HOST = os.environ['HER_DB_HOST'].strip()
E_ALLOWED_HOST = ""
if os.environ.get('HER_HOSTSALLOWED'):
    E_ALLOWED_HOST = os.environ['HER_HOSTSALLOWED'].strip()
E_GOOGLE_ANALYTICS_PROPERTY_ID = ""
if os.environ.get('HER_GOOGLE_ANALYTICS'):
    E_GOOGLE_ANALYTICS_PROPERTY_ID = os.environ['HER_GOOGLE_ANALYTICS'].strip()
E_WEBPORT = ""
if os.environ.get('HER_WEBPORT'):
    E_WEBPORT = os.environ['HER_WEBPORT'].strip().lower()
E_VERSION = "v5.0"

USE_LITE = True
if os.environ.get('HER_USE_LITE'):
    E_USE_LITE = os.environ['HER_USE_LITE'].strip().lower() == "true"
E_SECPX = False
if os.environ.get('HER_SECPX'):
    E_SECPX = os.environ['HER_SECPX'].strip().lower() == "true"
E_ISPROD = False
if os.environ.get('HER_PROD'):
    E_ISPROD = os.environ['HER_PROD'].strip().lower() == "true"
DEBUG = False
if os.environ.get('HER_DEBUG'):
    DEBUG = os.environ['HER_DEBUG'].strip().lower() == "true"
E_SHOWCONFIG = False
if os.environ.get('HER_SHOWCONFIG'):
    E_SHOWCONFIG = os.environ['HER_SHOWCONFIG'].strip().lower() == "true"

# SECURITY WARNING: don't run with debug turned on in production!
if E_ISPROD:
    DEBUG = False

#When DEBUG = False, Django doesn’t work at all without a suitable value for ALLOWED_HOSTS.
#ALLOWED_HOSTS = ['.hereditas.in']
ALLOWED_HOSTS = []
if E_ALLOWED_HOST and len(E_ALLOWED_HOST) > 0:
    ALLOWED_HOSTS = E_ALLOWED_HOST.split(',')

USE_LITE = True
if os.environ['HER_USE_LITE']:
    E_USE_LITE = os.environ['HER_USE_LITE'].strip().lower()
    if E_USE_LITE == "false":
        USE_LITE = False

if E_SHOWCONFIG:
    print("IO  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print("PROD: " + str(E_ISPROD))
    print("DEBUG: " + str(DEBUG))
    print("ALLOWED_HOSTS: " + ", ".join(ALLOWED_HOSTS))
    print("USE_LITE_SQL: " + E_USE_LITE)
    print("E_DB_NAME: " + E_DB_NAME)
    print("E_DB_USER: " + E_DB_USER)
    print("E_DB_HOST: " + E_DB_HOST)
    print("GOOGLE_ANALYTICS_PROPERTY_ID: " + E_GOOGLE_ANALYTICS_PROPERTY_ID)
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# usa 3 dirname para retornar ao root da aplicacao
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
# from .databaselocalsqlite3 import *
if USE_LITE:
    SQLLITEPATH = os.path.join(BASE_DIR, 'db.sqlite3')
    if not os.path.exists(SQLLITEPATH):
        os.mkdir(SQLLITEPATH)
    #SQL Lite 3
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(SQLLITEPATH, E_DB_NAME),
        }
    }
else:
    # PostgreSQL 9.4 Local Container Docker
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': E_DB_NAME,
            'USER': E_DB_USER,
            'PASSWORD': E_DB_PW,
            'HOST': E_DB_HOST,
            'PORT': '5432',
        },
        'dbadmshell': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'postgres',
            'USER': E_DB_USER,
            'PASSWORD': E_DB_PW,
            'HOST': E_DB_HOST,
            'PORT': '5432',
        }
     }

 # Google analytics variables
GOOGLE_ANALYTICS_PROPERTY_ID = ""
if E_GOOGLE_ANALYTICS_PROPERTY_ID:
    GOOGLE_ANALYTICS_PROPERTY_ID = E_GOOGLE_ANALYTICS_PROPERTY_ID

# SSL se o proxy realizar conexão segura com o web
#https://docs.djangoproject.com/es/1.9/ref/middleware/#http-strict-transport-security
#If this is set to True, client-side JavaScript will not to be able to access the session cookie.
#This can help prevent malicious JavaScript from bypassing CSRF protection. If you enable this and need to send the value of the CSRF token with Ajax requests, your JavaScript will need to pull the value from a hidden CSRF token form input on the page instead of from the cookie.
CSRF_COOKIE_NAME =  'csrfHereditas' # se alterar aqui, deve alterar no arquivo csrf.js

CSRF_COOKIE_HTTPONLY = True

if len(ALLOWED_HOSTS) > 0:
    CSRF_COOKIE_DOMAIN = ALLOWED_HOSTS[0]


# Modern browsers honor the X-Frame-Options HTTP header that indicates whether or not a resource is allowed to load within a frame or iframe
X_FRAME_OPTIONS = 'DENY'
#X_FRAME_OPTIONS = 'SAMEORIGIN'

# Turning it on makes it less trivial for an attacker to escalate a cross-site scripting vulnerability into full hijacking of a user’s session. 
# There’s not much excuse for leaving this off, either: if your code depends on reading session cookies from JavaScript, you’re probably doing it wrong.
SESSION_COOKIE_HTTPONLY = True

# Prevents Internet Explorer and Google Chrome from MIME-sniffing a response away from the declared content-type.
# This also applies to Google Chrome, when downloading extensions.
# This reduces exposure to drive-by download attacks and sites serving user uploaded content that, by clever naming, could be treated by MSIE as executable or dynamic HTML files.
#https://www.owasp.org/index.php/List_of_useful_HTTP_headers
SECURE_CONTENT_TYPE_NOSNIFF = True

#Cross-Site Scripting (XSS) attacks are a type of injection, in which malicious scripts are injected into otherwise benign and trusted web sites. XSS attacks occur when an attacker uses a web application to send malicious code, generally in the form of a browser side script, to a different end user. Flaws that allow these attacks to succeed are quite widespread and occur anywhere a web application uses input from a user within the output it generates without validating or encoding it.
#https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)
SECURE_BROWSER_XSS_FILTER = True

# Turn on if proxy is using SSL
SECURE_PROXY_SSL_HEADER = None
SECURE_HSTS_SECONDS = 0

if E_ISPROD and E_SECPX:
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
    # SSL if proxy doesn create a secure connection to web
    #https://docs.djangoproject.com/es/1.9/ref/middleware/#http-strict-transport-security
    SECURE_HSTS_SECONDS = 3600 #(1 hora)
    SECURE_SSL_REDIRECT = True # if not true it will get in redirect loop
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# usa 3 dirname para retornar ao root da aplicacao
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# SECURITY WARNING: keep the secret key used in production secret!
try:
    # tenta config do container
    with open('/etc/hereditas/sk') as f:
        SECRET_KEY = f.read().strip()
except FileNotFoundError:
    try:
        # tenta config local
        with open(BASE_DIR + '/sk') as f:
            SECRET_KEY = f.read().strip()
    except FileNotFoundError:
        print("erro: " + BASE_DIR)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
STATIC_URL = '/static/io/'

MEDIA_URL = '/media/io/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static/io')

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/io')

# REST permissions
REST_FRAMEWORK = {
    'DATETIME_FORMAT': "%d-%m-%Y %H:%M:%S",
    'DATE_FORMAT': "%d/%m/%Y",
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissions',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        # Assim garante que somente usuários com token podem usar
        'rest_framework.authentication.TokenAuthentication',
    ),
}