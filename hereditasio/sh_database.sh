#!/bin/bash

# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

Sintax(){
      echo "./sh_database.sh <user> <password>. Please inform database user and password."
      exit 1
}

hpgdata=$(docker ps --all | grep hpgdata)
hereditasdb=$(docker ps | grep hereditasdb)
removeCT=$(docker ps --all | grep hereditasdb)

# create a volume to store database files
if [ "${#hpgdata}" -eq 0 ]; then
  echo "creating database volume..."
  docker create -v /var/lib/postgresql/data/pgdata --name hpgdata postgres:alpine /bin/true
  sleep 2
fi

# check database server is running
if [ "${#hereditasdb}" -eq 0 ]; then
  # Remove existing database
  if [ "${#removeCT}" -ne 0 ]; then
    docker rm hereditasdb
  fi

  POSTGRES_USER=$1
  POSTGRES_PASSWORD=$2
  if [ -z "$POSTGRES_USER" ]; then
    Sintax
  fi

  if [ -z "$POSTGRES_PASSWORD" ]; then
    Sintax
  fi

  echo "starting postgres database..."
  docker run -d --name hereditasdb -e PGDATA=/var/lib/postgresql/data/pgdata -e POSTGRES_DB=postgres -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD --volumes-from hpgdata -p 5432:5432 postgres:alpine

  sleep 7

  if [ "${#hpgdata}" -eq 0 ]; then
    echo "creating database hereditas..."
    docker exec -it hereditasdb psql -U $POSTGRES_USER -d postgres -c "CREATE DATABASE hereditas WITH ENCODING='UTF8' OWNER=$POSTGRES_USER CONNECTION LIMIT=-1;"
  fi
fi