#!/bin/bash

# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

echo " "
echo "installing apt packages...."
sudo apt-get install python3-pip libpq-dev python3-dev
echo " "
echo "upgrading pip...."
sudo pip3 install --upgrade pip
echo " "
echo "locale gen...."
sudo locale-gen ${LANG}