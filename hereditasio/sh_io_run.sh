#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

if [ -z "$1" ]; then
    echo "./run.sh <waitsec>"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "Informar: 1 - Execução normal ## 2 - Reset migrations ## 3 - Fake migrations"
    exit 1
fi

echo " "
echo "Starting Hereditas IO..."
echo " "
echo "Mode $2"

echo "wating $1s"
sleep $1

if [ "$2" = "3" ]; then
    echo " "
    echo "Custom fake migrations..."
    python3 manage.py makemigrations --no-input hereditascore
    python3 manage.py migrate --fake --no-input hereditascore
fi

if [ "$2" = "2" ]; then
    echo " "
    echo "Reseting migrations..."
    echo "DELETE FROM django_migrations WHERE app = 'hereditascore'" |  python3 manage.py dbshell --database=default
    python3 manage.py makemigrations --no-input hereditascore
    python3 manage.py migrate --fake-initial --no-input hereditascore
fi

if [ "$2" = "1" ]; then
    echo " "
    echo "Running migrations..."
    python3 manage.py makemigrations --no-input
    python3 manage.py migrate --no-input
    
    echo " "
    echo "Checking super user"
    valor=$(echo "import sys; from django.contrib.auth.models import User;ret = str(len(User.objects.filter(email='$HER_ADMIN_EMAIL'))); print('r=' + ret)" | python3 manage.py shell | grep r=)
    valor="${valor:2:1}"
    if [ $valor -eq 0 ]; then
        echo " "
        echo "Creating super user..."
        echo "from django.contrib.auth.models import User; User.objects.create_superuser('$HER_ADMIN', '$HER_ADMIN_EMAIL', '$HER_ADMIN_PW')" | python3 manage.py shell
    fi    
fi

echo " "
echo "Running server..."
gunicorn hereditasio.wsgi:application -t 120 -w 1 -b 0.0.0.0:8000 --log-level=error
