from django.conf import settings
from django.http import HttpResponse

ACCESS_CONTROL_ALLOW_ORIGIN = 'Access-Control-Allow-Origin'
ACCESS_CONTROL_MAX_AGE = 'Access-Control-Max-Age'
ACCESS_CONTROL_ALLOW_HEADERS = 'Access-Control-Allow-Headers'
ACCESS_CONTROL_ALLOW_METHODS = 'Access-Control-Allow-Methods'

class HereditasIOCorsMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def process_request(self, request):
        """
        If CORS preflight header, then create an
        empty body response (200 OK) and return it
        Django won't bother calling any other request
        view/exception middleware along with the requested view;
        it will call any response middlewares
        """
        if getattr(settings, 'DEBUG', False):
            response = HttpResponse()
            response[ACCESS_CONTROL_ALLOW_ORIGIN] = "http://" + getattr(settings, 'E_ALLOWED_HOST') + ":" + getattr(settings, 'E_WEBPORT')
            response[ACCESS_CONTROL_ALLOW_HEADERS] = "Token, Content-Type, authorization"
            response[ACCESS_CONTROL_ALLOW_METHODS] = "POST, GET, OPTIONS, HEAD"
            response.status_code = 200
            return response


    def process_template_response(self, request, response):
        if getattr(settings, 'DEBUG', True):
            response[ACCESS_CONTROL_ALLOW_ORIGIN] = "http://" + getattr(settings, 'E_ALLOWED_HOST') + ":" + getattr(settings, 'E_WEBPORT')
            response[ACCESS_CONTROL_ALLOW_HEADERS] = "Token, Content-Type, authorization"
            response[ACCESS_CONTROL_ALLOW_METHODS] = "*"
            response.status_code = 200
            return response