#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

if [ -z "$1" ]
  then
    echo "Informar versão do build"
    exit 1
fi

arr=$(echo $1 | cut -d "." -f 2)

if [ $((arr%2)) -eq 0 ]
  then
    exit 10
  else
    exit 16
fi
