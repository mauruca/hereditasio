#!/bin/sh
# Copyright (C) 2015 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.
#
# Você deve ter recebido uma cópia da licença que está no arquivo LICENCA ou LICENSE. Se não,
# entrar em contato escrevendo para mpinheiro@pobox.com ou mauricio.pinheiro@gmail.com ou mauricio@ur2.com.br.

echo "Iniciando criação de $1 bens..."
echo "from patrimonio.bulk.bemCreate import *; criaBem($1)" | python3 manage.py shell
echo "Criação de $1 bens finalizada."
