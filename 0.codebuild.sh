#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
baseimage="$(cat baseimage)"
ver="$(cat version)"

docker build -f Dockerfile.build --build-arg baseimage -t hereditas/buildimage:$ver .
echo " "
echo "Running the build inside a container..."
echo " "
docker run --rm -it -u $(id -u):$(id -g) -e HOME=/build/env -e PATH=$PATH:/build/env/.local/bin -v $(pwd):/build hereditas/buildimage:$ver /bin/ash -c "cd /build;./sh_setup_virtualenv.sh;./sh_setup_environment.sh;./sh_install_packages.sh;./sh_static_build.sh"