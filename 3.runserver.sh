#!/bin/bash

# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

apiport="4000"
pgaport="8000"
webport="8080"
mode="run"
db="lite"

Sintaxe(){
    echo "./runserver.sh <db password> <hereditas admin password> <optional mode test|run> <optional database type lite|postgres>"
    exit 1
}

if [ ! -z "$1" ]; then
    HER_DB_PW="$1"
else
   Sintaxe
fi

if [ ! -z "$2" ]; then
    HER_ADMIN_PW="$2"
else
    Sintaxe
fi

if [ ! -z "$3" ]; then
    mode="$3"
fi

if [ ! -z "$4" ]; then
    db="$4"
fi



echo " "
echo "starting dev environment..."
if [ -d env ]; then
    rm -R env
fi
./sh_setup_virtualenv.sh
./sh_setup_environment.sh
./sh_install_packages.sh
source env/bin/activate

echo " "
echo "creating secret file..."
openssl rand -base64 50 > sk

# sec issues
export HER_DB_USER="herad"
export HER_DB_PW
export HER_SECPX="false"
export HER_ADMIN="had" 
export HER_ADMIN_PW
export HER_PROD="false"
export HER_DEBUG="true"
export HER_SHOWCONFIG="false"

# db - export this to allow cmd execution
export HER_USE_LITE="true"
export HER_DB_NAME="hereditas"
export HER_DB_HOST="localhost"

# hosts
export HER_APIPORT="$apiport"
export HER_WEBPORT="$webport"
HER_HOSTSALLOWED="localhost,127.0.0.1"
if [ ! -z "$HOSTNAME" ]; then
    HER_HOSTSALLOWED="$HER_HOSTSALLOWED,$HOSTNAME"
fi
export HER_HOSTSALLOWED
# google analytics
export HER_GOOGLE_ANALYTICS=""
export HER_ADMIN_EMAIL="admin@hereditas.in"

# postgresql option
if [ "$db" == "postgres" ]; then
  export HER_USE_LITE="false"

  hpgdata=$(docker ps --all | grep hpgdata)
  echo " "
  echo "starting container database..."
  hereditasio/sh_database.sh $HER_DB_USER $HER_DB_PW
fi

# update database
echo " "
echo "Build migrations..."
python3 manage.py makemigrations --noinput
python3 manage.py migrate

echo "Checking super user"
valor=$(echo "import sys; from django.contrib.auth.models import User;ret = str(len(User.objects.filter(email='$HER_ADMIN_EMAIL'))); print('r=' + ret)" | python3 manage.py shell | grep r=)
valor="${valor:2:1}"
if [ $valor -eq 0 ]; then
    echo "Creating super user..."
    echo "from django.contrib.auth.models import User; User.objects.create_superuser('$HER_ADMIN', '$HER_ADMIN_EMAIL', '$HER_ADMIN_PW')" | python3 manage.py shell
fi

# Run server or test option
if [ "$mode" == "test" ]; then
  echo " "
  echo "starting tests..."
  python3 manage.py test
else
  # admin login alert
  echo " "
  echo  -e "\033[31;5mAdministrator login: $HER_ADMIN and password: $HER_ADMIN_PW\033[0m"  
  echo " "
  echo -e "starting server... @ http://localhost:${HER_APIPORT}\n\n"
  python3 manage.py runserver 127.0.0.1:$HER_APIPORT
fi

# leave virtualenv
deactivate